*Table of Contents*

---

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [Development Sandbox](#development-sandbox)
  - [Prerequisites](#prerequisites)
  - [Recommendations](#recommendations)
    - [Language](#language)
    - [Mounted Volumes](#mounted-volumes)
    - [Bound Ports](#bound-ports)
  - [Setup sandbox with dockerfile](#setup-sandbox-with-dockerfile)
    - [Notes on code above:](#notes-on-code-above)

<!-- /code_chunk_output -->

---

# Development Sandbox

## Prerequisites

1.  [Bash shell](https://www.gnu.org/software/bash/)
2.  [Docker Community Edition](https://wiki.archlinux.org/index.php/Docker) or [Docker Desktop](https://www.docker.com/products/docker-desktop)

## Recommendations

### Language

The code below was made for a [golang](https://golang.org/) sandbox. However, it should work for any language that uses a [debian](https://www.debian.org/) based image, like Ubuntu. To change the language from golang to anything else desired, simply change the `lang` environment variable below. Which can be found in the `Variables` section.

### Mounted Volumes

This sandbox has been created with the intention of being able to mount the container's user home directory to the host computer inside this repo's directory ( `./home` ). This should make migrating work in and out of it easier. It is recommended to add this mounted volume to `.gitignore` and to version control this mounted volume separately, but you do you.

### Bound Ports

Change them if desired, the intentions was to do the minimum needed for development work

## Setup sandbox with dockerfile

git clone this repo and cd into it before preforming the following. **NOTE:** This has only been tested with Ubuntu (debian based) base images, and might not work on other base images.

```sh
#- Variables (change any as desired)
lang=golang
name="${lang}-dev"
tag='latest'
sandbox="$(pwd)"/home # if modified, update gitignore
mkdir -p "${sandbox}"

#- Docker - build, run, exec
docker build \
  --build-arg IMAGE="${lang}" \
  --build-arg TAG="${tag}" \
  --build-arg USER="${USER}" \
  -t "${name}":"${tag}" \
  -f Dockerfile .

docker run \
  -h "${name}" \
  -v "${sandbox}":/home/$USER \
  -w /home/$USER \
  -p 8080:80 \
  -p 8443:443 \
  -td \
  --name "${name}" \
  "${name}":"${tag}"

docker exec -ti "${name}" bash
```

### Notes on code above:

**NOTE:** If you start a new session and have not yet declared the environment variables, these commands will not work.

-  [docker build](https://docs.docker.com/engine/reference/commandline/build/)
    -  Set build-time variables with `--build-arg`

            --build-arg IMAGE="${lang}" # Set the base imgae to `$lang` (default is golang)
            --build-arg TAG="${tag}"    # Set image tag to `$tag` (default is latest)
            --build-arg USER            # Set `$USER` to be the host's `$USER`
-  [docker run](https://docs.docker.com/engine/reference/commandline/run/)
    -  Publish/bind/expose host ports to container's port. This allows for binding a webpage that the container is hosted for example on port `80` to the localhost on port `8080`, like so `127.0.0.1:8080`

            -p 8080:80  # Bind host port `8080` to container's port `80`
            -p 8443:443 # Bind host port `8443` to container's port `443`
    -  Allocate a pseudoterminal and detach. This allows the container to continusally run.

            -td
-  [docker exec](https://docs.docker.com/engine/reference/commandline/exec/)
    -  Run the container that matches `"${name}"` in interactive mode with the cmd `bash`

            docker exec -ti "${name}" bash
    -  Run the container using a different shell. This contianer is pre-configured with the [Zsh IMproved FrameWork](https://github.com/zimfw/zimfw) (Zim). You could also use `dash`

            docker exec -ti "${name}" zsh

- Since the sandbox is designed to stay running, you will have to stop the container manually with:

    ```sh
    docker stop "${name}"
    ```

- To start the sandbox up again after it has been stopped do the following:

    ```sh
    docker start "${name}"
    ```